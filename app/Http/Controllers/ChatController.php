<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;
use App\User;
use App\Events\NewChat;

class ChatController extends Controller
{
    public function index()
    {
        return view('chats');
    }

    public function getMessages()
    {
        $messages = Chat::all();
        return response()->json($messages);
    }

    public function createMessages(Request $resquest)
    {
        $user = User::find(1)->first();
        $message = new Chat;
        $message->author = $user->name;
        $message->content = $resquest->content;
        $message->save();
        event(
            new NewChat($message)
        );
        return response()->json($message,200);
    }
}
