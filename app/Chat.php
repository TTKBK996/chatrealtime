<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = ['author', 'content'];

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
