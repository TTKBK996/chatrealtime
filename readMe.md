1. composer install
2. npm install
3. .env
+++++++++++++++++++++++++++++++++++++++++
BROADCAST_DRIVER=pusher

PUSHER_APP_ID=id123
PUSHER_APP_KEY=key123
PUSHER_APP_SECRET=secret123
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
MIX_PUSHER_HOST=127.0.0.1
MIX_PUSHER_SCHEME=http

+++++++++++++++++++++++++++++++++++++++++
4. config \ broadcasting 

'pusher' => [
            'driver' => 'pusher',
            'key' => env('PUSHER_APP_KEY'),
            'secret' => env('PUSHER_APP_SECRET'),
            'app_id' => env('PUSHER_APP_ID'),
            'options' => [
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'encrypted' => true,
                'host' => '127.0.0.1',
                'port' => 6001,
                'scheme' => 'http'
            ],
        ],

5. config \ websocket
'apps' => [
        [
            'id' => env('PUSHER_APP_ID'),
            'name' => env('APP_NAME'),
            'key' => env('PUSHER_APP_KEY'),
            'secret' => env('PUSHER_APP_SECRET'),
            'path' => env('PUSHER_APP_PATH'),
            'capacity' => null,
            'enable_client_messages' => false,
            'enable_statistics' => true,
        ],
    ],
6. resources \ bootstrap js
import Echo from "laravel-echo"

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    wsHost: process.env.MIX_PUSHER_HOST,
    wsPort: 6001,
    forceTLS: false,
    disableStats: true,
    scheme: process.env.MIX_PUSHER_SCHEME
});

7. Eventn  * implements ShouldBroadcast *


=====================================================
*************   Run Project *********************
1. npm run watch
2. php artisan serve
3. php artisan websockets:serve 
-->  http:\\127.0.0.1/laravel-websockets : connect
=====================================================


